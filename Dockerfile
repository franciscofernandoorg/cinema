FROM openjdk:8-jdk-alpine
RUN apk add --no-cache maven
WORKDIR /java
COPY . /java
RUN mvn package -Dmaven.test.skip=true
EXPOSE 3000
ENTRYPOINT ["java","-jar","/java/target/api-0.0.1.jar"]