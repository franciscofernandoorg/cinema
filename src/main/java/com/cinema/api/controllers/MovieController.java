package com.cinema.api.controllers;

import com.cinema.api.models.Movie;
import com.cinema.api.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class MovieController {
    @Autowired
    private MovieRepository movieRepository;

    @GetMapping("/healthy")
    // @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody void healthy() {

    }

    @GetMapping("/movie")
    public @ResponseBody Iterable<Movie> getMovies() {
        return movieRepository.findAll();
    }

    @PostMapping(
        value = "/movie",
        consumes = {MediaType.APPLICATION_JSON_VALUE},
        produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public Movie addMovie(
        @RequestBody Movie movie
    ) {
        return movieRepository.save(movie);
    }

    @PatchMapping(
        value = "/movie",
        consumes = {MediaType.APPLICATION_JSON_VALUE},
        produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseStatus(HttpStatus.OK)
    public void patchMovie(
            @RequestBody Movie movie
    ) {

    }
}
